-- 1. Выведите все позиций списка товаров принадлежащие какой-либо категории с названиями товаров и названиями категорий. Список должен быть отсортирован по названию товара, названию категории. Для соединения таблиц необходимо использовать оператор INNER JOIN.
select good.name as good_name, c_t.name as category_name 
from good
inner join category_has_good on category_has_good.good_id = id
inner join category as c_t on category_has_good.category_id = category.id
order by good_name, category_name;

-- 2.Выведите список клиентов (имя, фамилия) и количество заказов данных клиентов, имеющих статус "new".
select first_name, last_name, count(status.id) as new_sale_num
from client
inner join sale on sale.client_id = client.id
inner join status on sale.status_id = status.id
where status.name = "new"
group by client.id;

-- 3. Выведите список товаров с названиями товаров и названиями категорий, в том числе товаров, не принадлежащих ни одной из категорий.
select good.name as good_name, category.name as category_name
from category_has_good
right join good on good_id = good.id
left join category on category_id = category.id 
order by good_name, category_name;

-- 4. Выведите список всех источников клиентов и суммарный объем заказов по каждому источнику. Результат должен включать также записи для источников, по которым не было заказов.
select source.name, sum(sale.sale_sum) as sale_sum
from client 
right join source on source.id = client.source_id
left join sale on sale.client_id = client.id
group by source.name
order by sale_sum desc;

-- 5. Выведите названия товаров, которые относятся к категории 'Cakes' или фигурируют в заказах текущий статус которых 'delivering'. Результат не должен содержать одинаковых записей. В запросе необходимо использовать оператор UNION для объединения выборок по разным условиям.
select good.name as good_name
from good
inner join category_has_good on good.id = category_has_good.good_id
inner join category on category.id = category_has_good.category_id 
where category.name = "cakes"

union 

select good.name as good_name
from good
inner join sale_has_good on good.id = sale_has_good.good_id
inner join sale on sale.id = sale_has_good.sale_id
inner join status on status.id = sale.status_id
where status.name = "delivering";

-- 6. Выведите список всех категорий продуктов и количество продаж товаров, относящихся к данной категории. Под количеством продаж товаров подразумевается суммарное количество единиц товара данной категории, фигурирующих в заказах с любым статусом.
select category.name, count(sale_id) as sale_num
from sale_has_good
right join good on sale_has_good.good_id = good.id -- Если у какого-то товара нету продажы - он нужен, а продажы без товаров не нужны
right join category_has_good on good.id = category_has_good.good_id -- Если у товара нет катеогории он не нужен, если у категории нет товара она нужна
right join category on category_has_good.category_id = category.id -- Нужны все категории
group by category.name -- Group by по category.id даёт не все существующие названия категорий
order by sale_num desc;

-- 7. Выведите список источников, из которых не было клиентов, либо клиенты пришедшие из которых не совершали заказов или отказывались от заказов. Под клиентами, которые отказывались от заказов, необходимо понимать клиентов, у которых есть заказы, которые на момент выполнения запроса находятся в состоянии 'rejected'. В запросе необходимо использовать оператор UNION для объединения выборок по разным условиям.
select source.name
from source
left join client on source.id = client.source_id
where isnull(client.id) or client.id in (
	select client.id
	from client 
	left join sale on client.id = sale.client_id
	left join status on status.id = sale.status_id
	where isnull(sale.id) or status.name = "rejected"
);



