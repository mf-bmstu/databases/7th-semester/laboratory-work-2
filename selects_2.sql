-- 1. Разбить 7-ой запрос из изначального задания на 3 отедльных запроса и сравнить результаты
-- 1.1 Выведите список источников, из которых не было клиентов
select source.name
from source
where not exists (
	select *
    from client
    where client.source_id = source.id
);


-- 1.2 Клиенты которые не совершали заказов
begin;

select * from source;

insert into client (id , code, first_name, last_name, source_id)
values (11, 'client_7', 'Harry ', 'Monroe', 7);

select source.name
from source
where exists(
	select client.id
	from client 
	left join sale on client.id = sale.client_id
	where isnull(sale.id) and client.source_id = source.id
);

rollback;


-- 1.3 Источники клиенты пришедшие из которых отказывались от заказов.
select source.name
from source
where id in (
	select client.source_id
	from client 
	left join sale on client.id = sale.client_id
	left join status on status.id = sale.status_id
    where status.name = "rejected"
);


-- 2. Посчитать количество уникальных категорий для каждого источника
select source.name as source_name, count(distinct category_has_good.category_id) as unique_category_count
from source
left join client on client.source_id = source.id
left join sale on sale.client_id = client.id
left join sale_has_good on sale_has_good.sale_id = sale.id
left join good on good.id = sale_has_good.good_id
left join category_has_good on category_has_good.good_id = good.id
group by source.id
order by unique_category_count desc;


-- 3. Вывести названия товаров и их катеогорий для товаров, принадлежащим больше чем одной категории
select goods.name as good_name, category.name as category_name
from (
	select good.id, good.name
	from good
	left join category_has_good on category_has_good.good_id = good.id
	group by good.id
	having count(category_has_good.category_id) > 1
) as goods
left join category_has_good on category_has_good.good_id = goods.id
left join category on category.id = category_has_good.category_id
order by goods.name;








    
  